require 'json'
require 'ninety_seconds/database/document'

module NinetySeconds
  module Database
    extend self

    DATA_DIRECTORY = "#{Dir.pwd}/data"
    @data = []

    def load_from_directory(directory)
      Dir[directory + '/*.json'].each do |file|
        load_from_file(file)
      end
    end

    def load_from_file(file)
      content = File.read(file)
      jsons = JSON.parse(content)
      documents = jsons.map { |json| Document.new(json) }
      @data.push(*documents)
    rescue StandardError
    end

    def search(conditions)
      results = []
      @data.select do |document|
        results << document.to_hash if document.match?(conditions)
      end
      results
    end
  end

  Database.load_from_directory(Database::DATA_DIRECTORY)
end
