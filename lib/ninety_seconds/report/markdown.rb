module NinetySeconds
  module Report
    class Markdown

      def initialize(objects)
        @objects = objects
      end

      def prepare
        [file_path, file_content]
      end

      private

      attr_reader :objects

      def file_path
        "report-#{Time.now.to_i}.md"
      end

      def file_content
<<-MARKDOWN
### Report

#{objects.length == 0 ? 'There is no matching for your query' : objects_template}
MARKDOWN
      end

      def objects_template
<<-MARKDOWN

There are #{objects.length} matching items:

#{objects.map.with_index { |object, index| object_template(index, object) }.join("\n")}
MARKDOWN
      end

      def object_template(index, object)
<<-MARKDOWN

#{index + 1}. #{object["name"]}
  ```
  #{object}
  ```
MARKDOWN
      end
    end
  end
end
