require 'ninety_seconds/search'

module NinetySeconds
  module Cli
    extend self

    def start(args)
      options =
        args.inject({}) do |options, exp|
          key, value = exp.split('=')
          options[key] = value
          options
        end
      message = Search.new(options).call
      puts message
    end
  end
end
