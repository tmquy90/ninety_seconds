module NinetySeconds
  module Database
    module Compare
      extend self

      def call(origin, target)
        klass = origin.class

        if origin.instance_of?(TrueClass) || origin.instance_of?(FalseClass)
          origin.to_s == target
        elsif origin.instance_of?(Array)
          origin.include?(target)
        else
          origin == target
        end
      end
    end
  end
end
