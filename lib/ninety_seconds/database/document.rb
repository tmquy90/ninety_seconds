require 'ninety_seconds/database/compare'

module NinetySeconds
  module Database
    class Document

      def initialize(json)
        @json = json
      end

      def match?(conditions)
        conditions.all? do |key, value|
          Compare.call(json[key], value)
        end
      end

      def to_hash
        json
      end

      private

      attr_reader :json
    end
  end
end
