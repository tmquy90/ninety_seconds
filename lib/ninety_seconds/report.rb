require 'ninety_seconds/report/markdown'

module NinetySeconds
  module Report
    extend self

    def print(objects)
      file_path, file_content = Markdown.new(objects).prepare
      file = File.open(file_path, 'w')
      file.write(file_content)
      "The report #{file_path} was created successfully."
    end
  end
end
