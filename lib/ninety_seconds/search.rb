require 'ninety_seconds/database'
require 'ninety_seconds/report'

module NinetySeconds
  class Search
    def initialize(conditions)
      @conditions = conditions
    end

    def call
      objects = Database.search(conditions)
      Report.print(objects)
    end

    private

    attr_reader :conditions
  end
end
