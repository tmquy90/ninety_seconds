## 90Seconds

### Test the project
+ Source code: https://bitbucket.org/tmquy90/ninety_seconds
+ Or watch online at youtube: https://www.youtube.com/watch?v=nwJUjrIqoqw

### Protect Timelime

+ Idea/Boilterplate: 21:00 -> 21:30 Aug 18
+ Implement Code: 21:30 -> 23:30 Aug 18
+ Document: 23:30 -> 00:00 Aug 19

### How to setup the project

+ Open terminal
+ `git clone https://bitbucket.org/tmquy90/ninety_seconds & cd ninety_seconds`
+ run `gem install bundler` and `bundle install`
+ run `./bin/ninety_seconds active=true`, it will create a new report-xxx.md for example `report-1501834532.md` in the project

__Run tests__
+ run `rspec spec`

### How about the project

#### Structure

```
├── ninety_seconds                    # 90Seconds source code
│   ├── database                      # Load and compare data
│   ├── report                        # Create a report file
│   └── search                        # Get data from database, query based on conditions and pass results into report
```
