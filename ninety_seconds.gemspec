$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ninety_seconds/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ninety_seconds"
  s.version     = NinetySeconds::VERSION
  s.authors     = ["Minh Quy"]
  s.email       = ["sugiacupit@gmail.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]
end
