require 'spec_helper'

describe NinetySeconds::Database::Document do
  subject { described_class }

  context '#match?' do
    it 'returns true when having key=name value=hello' do
      document = subject.new({name: "hello"})
      expect(document.match?({name: "hello"})).to be_truthy
    end

    it 'returns false when not having all' do
      document = subject.new({name: "hello"})
      expect(document.match?({name: "hello", id: "23"})).to be_falsey
    end
  end
end
