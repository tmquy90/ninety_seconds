require 'spec_helper'

describe NinetySeconds::Database::Compare do
  subject { described_class }

  context '.call' do
    it 'returns true for boolean and string boolean' do
      expect(subject.call(true, 'true')).to be_truthy
    end

    it 'returns true if element inside array' do
      expect(subject.call([1, 2], 1)).to be_truthy
    end

    it 'returns true if element is equal' do
      expect(subject.call("hello", "hello")).to be_truthy
    end

    it 'returns fasle if element is different' do
      expect(subject.call("hello", "heloo")).to be_falsey
    end
  end
end
