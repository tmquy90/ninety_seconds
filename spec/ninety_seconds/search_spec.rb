require 'spec_helper'

describe NinetySeconds::Search do
  subject { described_class }

  context '#call' do
    it 'returns message' do
      expect(NinetySeconds::Database).to receive(:search).and_return([])
      expect(NinetySeconds::Report).to receive(:print).and_return("Hello")
      message = subject.new({}).call
      expect(message).to eql("Hello")
    end
  end
end
