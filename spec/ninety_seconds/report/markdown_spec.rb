require 'spec_helper'

describe NinetySeconds::Report::Markdown do
  subject { described_class }

  context '#prepare' do
    it 'returns empty content' do
      file_path, file_content = subject.new([]).prepare
      expect(file_content).to include('no matching')
    end

    it 'returns one object' do
      file_path, file_content = subject.new([{ name: "Ninety Seconds" }]).prepare
      expect(file_content).to include('Ninety Seconds')
    end
  end
end
