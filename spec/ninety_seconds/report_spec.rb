require 'spec_helper'

describe NinetySeconds::Report do
  subject { described_class }

  context '.print' do
    it 'returns successful message' do
      allow_any_instance_of(NinetySeconds::Report::Markdown).to receive(:prepare).and_return('report.md', 'Ninety Seconds')
      file = spy("file")
      expect(File).to receive(:open).with('report.md', 'w').and_return(file)
      message = subject.print({})
      expect(file).to have_received(:write)
      expect(message).to eql("The report report.md was created successfully.")
    end
  end
end

