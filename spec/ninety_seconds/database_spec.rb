require 'spec_helper'

describe NinetySeconds::Database do
  subject { described_class }

  context '.search' do
    it 'returns one object' do
      objects = subject.search({ "locale" => "en-AU" })
      expect(objects.length).to eql(1)
    end
  end

  context '.load_from_file' do
    it 'raise silent error and do nothing' do
      expect {
        subject.load_from_file('xczxc')
      }.not_to raise_error
    end

    it 'adds one more document into data' do
      expect {
        subject.load_from_file(texture_path('users.json'))
      }.to change { subject.instance_variable_get(:@data).length }.by(1)
    end
  end

  context '.load_from_directory' do
    it 'calls load_from_file twice' do
      expect(subject).to receive(:load_from_file).once
      subject.load_from_directory(texture_directory)
    end
  end
end
