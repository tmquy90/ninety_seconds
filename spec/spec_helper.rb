require "rubygems"
require "bundler/setup"

require "ninety_seconds"

RSpec.configure do |config|
  config.color = true
end

def texture_path(filename)
  File.join(File.dirname(__FILE__), 'fixtures', "#{filename}")
end

def texture_content(filename)
  File.read File.join(File.dirname(__FILE__), 'fixtures', "#{filename}")
end

def texture_directory
  File.join(File.dirname(__FILE__), 'fixtures')
end
